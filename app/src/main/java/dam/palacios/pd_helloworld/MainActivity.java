package dam.palacios.pd_helloworld;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends LogActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupUI();
    }

    private void setupUI() {
        Button btNextActivity = findViewById(R.id.btNextActivity1);

        btNextActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,NextActivity.class));
            }
        });
    }

    public void launchNextActivity(View view) {
        startActivity(new Intent(this,NextActivity.class));
    }
}